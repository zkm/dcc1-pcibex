// This is a simple demo script, feel free to edit or delete it
// Find a tutorial and the list of availalbe elements at:
// https://www.pcibex.net/documentation/

PennController.ResetPrefix(null) // Shorten command names (keep this line here)
PennController.DebugOff() // Comment out to use Debugger

// Set counter at beginning so that simulatenous participants don't get the same group assignment
SetCounter("counter", "inc", 1);

// Show the 'intro' trial first, then all the 'experiment' trials in a random order
// then send the results and finally show the trial labeled 'bye'
Sequence("counter", "consent",  "training-instr", "training", "attention",
    "exp-instr", "experiment_block1", "exp-instr2", "experiment_block2",
    "guiseq", "demo_html", "region", "comment_page", SendResults() , "bye" )
//Sequence("region", "comment_page", SendResults(), "bye") // troubleshooting sequence
PennController.AddHost("http://terpconnect.umd.edu/~zach/dcc/")

// What is in Header happens at the beginning of every single trial
Header(
    // We will use this global Var element later to store the participant's name
    newVar("ParticipantName")
        .global()
    ,
    // Delay of 250ms before every trial
    newTimer(250)
        .start()
        .wait()
)
//.log( "Name" , getVar("ParticipantName") )
// This log command adds a column reporting the participant's name to every line saved to the results

//Consent
PennController("consent",
     newHtml("consent-form", "consent.html")
        .print()
     ,

     //log Prolific ID
     newText("Prolific ID:")
        .bold()
        .center()
        .print()
    ,
    newTextInput("prolificID")
        .settings.log()
        .settings.lines(1)
        .center()
        .print()
        .log()
    ,
    newText(" ")
        .print()
    ,
     newButton("consent-button", "I consent.")
        .center()
        .print()
        .wait()
);

// Training Instructions
PennController("training-instr",
     newHtml("instructions_training.html")
        .print()
     ,
     newButton("Start Practice")
        .center()
        .print()
        .wait()
);


// Declare variables for image and canvas size in SPR
let img_dim = 175;
let box_dim = 185;
let spr_canvas_dim = 400;
let img_left = 5;
let img_right = 220;
let img_top = 5;
let img_bottom = 220;
let canv_width = 598;
let canv_height = 308;

// Speech bubble image
let sb_width = 391;
let sb_height = 308;
let sb_x_left = 0;
let sb_x_right = 207;

// DashedSentence Placement
let ds_x_left = 240;
let ds_x_right = 240;
let ds_y = -10;

let guise_name_x_left = 80;
let guise_name_x_right = 390;
let guise_name_y = 200;


// Training
Template( "dcc1_training.csv" ,
    row => newTrial( "training",
        newText("trainInstr1", "Press the space bar to read each word.\n\n")
            .italic()
            .center()
            .print()
        ,    
        newImage("sb1", "training_sb_inplace.png")
            .settings.size(sb_width, sb_height)
        ,
        newCanvas("sprcanv", canv_width, canv_height)
            .center()
            .add( sb_x_left, 0, getImage("sb1"))
            .print()
        ,
        newController("DashedSentence", {s: row.SentenceText, blankText: "+", display: "in place"}) // add display: "in place" for non-moving window
            .print( ds_x_left, ds_y , getCanvas("sprcanv") ) 
            .log()
            .wait()
            .remove()
        ,
        getCanvas("sprcanv").remove(),
        getText("trainInstr1").remove()
    ,
   // Picture verification                 
    newImage('imageTL' ,  row.ImageTL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageTR' ,  row.ImageTR)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBL' ,  row.ImageBL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBR' ,  row.ImageBR)
        .settings.size(img_dim, img_dim)
    ,
    newText("trainInstr2", "Select the image that matches the sentence you just read.\n\n")
        .italic()
        .center()
        .print()
    ,    
    newCanvas( 'myCanvas', spr_canvas_dim, spr_canvas_dim)
        .settings.add( img_left, img_top, getImage('imageTL'), 0 )
        .settings.add( img_right, img_top, getImage('imageTR'), 1 )
        .settings.add( img_right, img_bottom, getImage('imageBR'), 2 )
        .settings.add( img_left, img_bottom, getImage('imageBL'), 3 )
        .print()
     ,

    // Log canvas reaction time
    newVar("RT").global().set( v => Date.now() )
    ,
    newSelector( 'mySelector')
        .add( getImage('imageTL') , getImage('imageBL') , getImage('imageBR') , getImage('imageTR') )
        .wait()
        .log()
    ,
    getVar("RT").set( v => Date.now() - v )
    ,
    // Feedback
    newImage('greenRect', "green_rectangle.png")
        .settings.size(box_dim, box_dim)
        .print(row.CorrectX, row.CorrectY, getCanvas("myCanvas"))
    ,
    newText("Sentence:")
        .bold()
        .center()
        .print()
    ,
    newText('feedbackText', row.SentenceText)
        .center()
        .print()
    ,
    newButton("trainingButton", "Next")
        .center()
        .print()
        .wait()
    )
    .log("List", "Training")
    .log("FirstGuise", "Training")
    .log("Guise", "Training")
    .log("ItemType", "Training")
    .log("ItemID", "Training")
    .log( "Cond", "Training")
    .log( "SentenceText", row.SentenceText)
    .log( "ImageTL", row.ImageTL)
    .log( "ImageTR", row.ImageTR)
    .log( "ImageBL", row.ImageBL)
    .log( "ImageBR", row.ImageBR)
    .log( "LabelTL", row.LabelTL)
    .log( "LabelTR", row.LabelTR)
    .log( "LabelBL", row.LabelBL)
    .log( "LabelBR", row.LabelBR)
    .log( "ReactionTime", getVar("RT"))

)

// Attention check
newTrial( "attention",
        newText("trainInstr1", "Press the space bar to read each word.\n\n")
            .italic()
            .center()
            .print()
        ,    
        newImage("sb1", "training_sb_inplace.png")
            .settings.size(sb_width, sb_height)
        ,
        newCanvas("sprcanv", canv_width, canv_height)
            .center()
            .add( sb_x_left, 0, getImage("sb1"))
            .print()
        ,
        newController("DashedSentence", {s: "Please select three to show you are paying attention.", blankText: "+", display: "in place"}) // add display: "in place" for non-moving window
            .print( ds_x_left, ds_y , getCanvas("sprcanv") ) 
            .log()
            .wait()
            .remove()
        ,
        getCanvas("sprcanv").remove(),
        getText("trainInstr1").remove()
    ,
   // Picture verification                 
    newImage('imageTL' ,  "one.png")
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageTR' ,  "two.png")
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBL' ,  "three.png")
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBR' ,  "four.png")
        .settings.size(img_dim, img_dim)
    ,
    newText("trainInstr2", "Select the image that matches the sentence you just read.\n\n")
        .italic()
        .center()
        .print()
    ,    
    newCanvas( 'myCanvas', spr_canvas_dim, spr_canvas_dim)
        .settings.add( img_left, img_top, getImage('imageTL'), 0 )
        .settings.add( img_right, img_top, getImage('imageTR'), 1 )
        .settings.add( img_right, img_bottom, getImage('imageBR'), 2 )
        .settings.add( img_left, img_bottom, getImage('imageBL'), 3 )
        .print()
     ,

    // Log canvas reaction time
    newVar("RT").global().set( v => Date.now() )
    ,
    newSelector( 'mySelector')
        .add( getImage('imageTL') , getImage('imageBL') , getImage('imageBR') , getImage('imageTR') )
        .wait()
        .log()
    ,
    getVar("RT").set( v => Date.now() - v )
    ,
    newText("positive", "You passed this attention check!")  // Creation of a positive feedback text element
        .color("green")  // Green for positive (don't print yet)
        .css("font-size", "2em")
    ,
    newText("negative", "You failed the attention check. Please return the task in order to avoid having your submission rejected. Thank you!")   // Creation of a negative feedback text element
        .color("red")    // Red for negative (don't print yet)
        .css("font-size", "2em")
    ,
    getSelector("mySelector")           // Test whether the target image was selected
        .test.selected( getImage("imageBL") )
        .success(
            getText("positive")   // Positive feedback if the test succeeds
                .print()
        )
        .failure(
            getText("negative")   // Negative feedback if the test fails
                .print()
        )
   ,
    newButton("trainingButton", "Next")
        .center()
        .print()
        .wait()
    )
    .log("List", "Attention")
    .log("FirstGuise", "Attention")
    .log("Guise", "Attention")
    .log("ItemType", "Attention")
    .log("ItemID", "Attention")
    .log( "Cond", "Attention")
    .log( "SentenceText", "Attention")
    .log( "ImageTL", "Attention")
    .log( "ImageTR", "Attention")
    .log( "ImageBL", "Attention")
    .log( "ImageBR", "Attention")
    .log( "LabelTL", "Attention")
    .log( "LabelTR", "Attention")
    .log( "LabelBL", "Attention")
    .log( "LabelBR", "Attention")
    .log( "ReactionTime", getVar("RT"))


// Experiment instructions
PennController("exp-instr",
     newHtml("instructions_person1.html")
        .print()
     ,
     newButton("Start Experiment")
        .center()
        .print()
        .wait()
);

// Experimental trials - block 1

Template( "dcc1_guise1.csv" ,
    row => newTrial( "experiment_block1",
        newImage("sb1", "personA_left_inplace.png")
            .settings.size(sb_width, sb_height)
        ,
        newText("guise1_name", "Taylor")
        ,
        newCanvas("sprcanv", canv_width, canv_height)
            .center()
            .add( sb_x_left, 0, getImage("sb1"))
            // .add( guise_name_x_left, guise_name_y, getText("guise1_name"))
            .print()
        ,
        newController("DashedSentence", {s: row.Sentence, blankText: "+", display: "in place"}) // add display: "in place" for non-moving window
            .print( ds_x_left, ds_y , getCanvas("sprcanv") ) 
            .log()
            .wait()
            .remove()
        ,
        getCanvas("sprcanv").remove()
    ,
   // Picture verification                 
    newImage('imageTL' ,  row.ImageTL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageTR' ,  row.ImageTR)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBL' ,  row.ImageBL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBR' ,  row.ImageBR)
        .settings.size(img_dim, img_dim)
    ,
    newCanvas( 'myCanvas', spr_canvas_dim, spr_canvas_dim)
        .settings.add( img_left, img_top, getImage('imageTL'), 0 )
        .settings.add( img_right, img_top, getImage('imageTR'), 1 )
        .settings.add( img_right, img_bottom, getImage('imageBR'), 2 )
        .settings.add( img_left, img_bottom, getImage('imageBL'), 3 )
        .print()
     ,
    // Log canvas reaction time
    newVar("RT").global().set( v => Date.now() )
    ,
    newSelector( 'mySelector')
        .add( getImage('imageTL') , getImage('imageBL') , getImage('imageBR') , getImage('imageTR') )
        .wait()     
        .log()
    ,
    getVar("RT").set( v => Date.now() - v )
    )
    .log("List", row.Group)
    .log("FirstGuise", row.FirstGuise)
    .log("Guise", row.Guise)
    .log("ItemType", row.ItemType)
    .log("ItemID", row.ItemID)
    .log( "Cond", row.Cond)
    .log( "SentenceText", row.Sentence)
    .log( "ImageTL", row.ImageTL)
    .log( "ImageTR", row.ImageTR)
    .log( "ImageBL", row.ImageBL)
    .log( "ImageBR", row.ImageBR)
    .log( "LabelTL", row.LabelTL)
    .log( "LabelTR", row.LabelTR)
    .log( "LabelBL", row.LabelBL)
    .log( "LabelBR", row.LabelBR)
    .log( "ReactionTime", getVar("RT"))
)

// Experiment instructions
PennController("exp-instr2",
     newHtml("instructions_person2.html")
        .print()
     ,
     newButton("Start Part 2")
        .center()
        .print()
        .wait()
);

// Experimental trials - block 2
Template( "dcc1_guise2.csv" ,
    row => newTrial( "experiment_block2",
        newImage("sb1", "personB_right_inplace.png")
            .settings.size(sb_width, sb_height)
        ,
        newText("guise2_name", "Jordan")
        ,
        newCanvas("sprcanv", canv_width, canv_height)
            .center()
            .add( sb_x_right, 0, getImage("sb1"))
            // .add( guise_name_x_right, guise_name_y, getText("guise2_name"))
            .print()
        ,
        newController("DashedSentence", {s: row.Sentence, blankText: "+", display: "in place"}) // add display: "in place" for non-moving window
            .print( ds_x_right, ds_y , getCanvas("sprcanv") ) 
            .log()
            .wait()
            .remove()
        ,
        getCanvas("sprcanv").remove()
    ,
   // Picture verification                 
    newImage('imageTL' ,  row.ImageTL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageTR' ,  row.ImageTR)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBL' ,  row.ImageBL)
        .settings.size(img_dim, img_dim)
    ,
    newImage('imageBR' ,  row.ImageBR)
        .settings.size(img_dim, img_dim)
    ,
    newCanvas( 'myCanvas', spr_canvas_dim, spr_canvas_dim)
        .settings.add( img_left, img_top, getImage('imageTL'), 0 )
        .settings.add( img_right, img_top, getImage('imageTR'), 1 )
        .settings.add( img_right, img_bottom, getImage('imageBR'), 2 )
        .settings.add( img_left, img_bottom, getImage('imageBL'), 3 )
        .print()
     ,
    // Log canvas reaction time
    newVar("RT").global().set( v => Date.now() )
    ,
    newSelector( 'mySelector')
        .add( getImage('imageTL') , getImage('imageBL') , getImage('imageBR') , getImage('imageTR') )
        .wait()     
        .log()
    ,
    getVar("RT").set( v => Date.now() - v )
    )
    .log("List", row.Group)
    .log("FirstGuise", row.FirstGuise)
    .log("Guise", row.Guise)
    .log("ItemType", row.ItemType)
    .log("ItemID", row.ItemID)
    .log( "Cond", row.Cond)
    .log( "SentenceText", row.Sentence)
    .log( "ImageTL", row.ImageTL)
    .log( "ImageTR", row.ImageTR)
    .log( "ImageBL", row.ImageBL)
    .log( "ImageBR", row.ImageBR)
    .log( "LabelTL", row.LabelTL)
    .log( "LabelTR", row.LabelTR)
    .log( "LabelBL", row.LabelBL)
    .log( "LabelBR", row.LabelBR)
    .log( "ReactionTime", getVar("RT"))
)
// Questions about the guises
newTrial("guiseq",
    newText("Thank you for completing the main part of the experiment!\nWe now have a few questions about your background.\nThe first few questions ask about the language from the people who were \"talking\" in the experiment.\n")
        .center()
        .italic()
        .print()
    ,
    // GUISE 1
    // Display guise 1 image as a reminder
    newImage("guise1_avatar", "personA.png")
        .settings.size(100,100)
    ,
    newCanvas("guise1_reminder", 150, 100)
        .settings.add(0, 50, newText("Taylor"))
        .settings.add(50, 0, getImage("guise1_avatar"))
        .print()
    ,

    newText("p1interloc-text", "On a scale from 1 to 5, how often do you interact with people who talk like <i>Taylor</i> (the first person)?")
        .bold()
        .print()
    ,
    newScale("p1interloc",  "1 - Never", "2", "3 - Sometimes", "4", "5 - All the time")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText("p1self-text", "On a scale from 1 to 5, to what degree do <i>you</i> talk like <i>Taylor</i> (the first person)?")
        .bold()
        .print()
    ,
    newScale("p1self",  "1 - I never talk like this", "2", "3 - I sometimes talk like this", "4", "5 - I usually talk like this")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,

    // GUISE 2
    newImage("guise2_avatar", "personB.png")
        .settings.size(100,100)
    ,
    newCanvas("guise2_reminder", 150, 100)
        .settings.add(0, 50, newText("Jordan"))
        .settings.add(50, 0, getImage("guise2_avatar"))
        .print()
    ,
    newText("p2interloc-text", "On a scale from 1 to 5, how often do you interact with people who talk like <i>Jordan</i> (the second person)?")
        .bold()
        .print()
    ,
    newScale("p2interloc",  "1 - Never", "2", "3 - Sometimes", "4", "5 - All the time")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText("p2self-text", "On a scale from 1 to 5, to what degree do <i>you</i> talk like <i>Jordan</i> (the second person)?")
        .bold()
        .print()
    ,
    newScale("p2self",  "1 - I never talk like this", "2", "3 - I sometimes talk like this", "4", "5 - I usually talk like this")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newButton("guiseq-next", "Next")
        .print()
        .wait()

)

// Demographics using pcIBEX (changed for checkboxes in HTML)
// newTrial("demographics",
//     newText("Background information about you\n\n")
//         .center()
//         .bold()
//         .italic()
//         .print()
//     ,
//     newText("age-text", "What is your age?")
//         .bold()
//         .print()
//     ,
//     newScale("age",  "18-24", "25-29", "30-39", "40-49", "50-59", "60+")
//         .settings.labelsPosition("right")
//         .settings.vertical()
//         .print()
//     ,
//     newText("What is your race/ethnicity?")
//         .bold()
//         .print()
//     ,
//     newScale("race", "White, non-Hispanic", "Hispanic", "Black/African American", "Asian", "Native American/Alaskan Native", "Other", "Prefer not to disclose")
//         .settings.labelsPosition("right")
//         .settings.vertical()
//         .print()
//     ,
//     newText("What is the highest level of education you completed?")
//         .bold()
//         .print()
//     ,
//     newScale("ed", "Less than high school diploma", "High school diploma or equivalent", "Associate's/Technical degree, trade school, or some college", "4-year college degree", "Graduate school") 
//         .settings.labelsPosition("right")
//         .settings.vertical()
//         .print()
//     ,
//     newText("If \"less than high school diploma\", list highest grade completed (for example, \"11th grade\"):")
//         .italic()
//         .print()
//     ,
//     newTextInput("highestGrade")
//         .settings.log()
//         .settings.lines(1)
//         .print()
//     ,
//     newText("If \"some college\", list how many years completed:")
//         .italic()
//         .print()
//     ,
//     newTextInput("collegeYears")
//         .settings.log()
//         .settings.lines(1)
//         .print()
//     ,
//     newText("Are you a native speaker of English?")
//         .bold()
//         .print()
//     ,
//     newScale("englishNative", "Yes", "No") 
//         .settings.labelsPosition("right")
//         .settings.vertical()
//         .print()
//     ,
//     newText("Were any other languages spoken in your household growing up?")
//         .bold()
//         .print()
//     ,
//     newScale("childOtherLg", "Yes", "No") 
//         .settings.labelsPosition("right")
//         .settings.vertical()
//         .print()
//     ,
//     newText("If \"Yes\", which other language(s? was/were spoken?")
//         .italic()
//         .print()
//     ,
//     newTextInput("childOtherLgList")
//         .settings.log()
//         .settings.lines(1)
//         .print()
//     ,
//     newText("Please list any other language(s) you speak:")
//         .bold()
//         .print()
//     ,
//     newTextInput("otherLg")
//         .settings.log()
//         .settings.lines(1)
//         .print()
//     ,
//     newButton("dem-next", "Next")
//         .print()
//         .wait()
// )

newTrial("demo_html",
    newHtml("demographics.html")
        .log()
        .print()
    ,
    newButton("demo_next", "Next")
        .print()
        .wait()
)

newTrial("region",
    newText("Our final questions are about places you have lived.\nThis information is helpful for our research and will be kept confidential, but these questions are optional.\n")
        .italic()
        .print()
    ,  
    //Current Location 
    newText("Where do you currently live?")
        .bold()
        .print()
    ,
    newDropDown("currentState", "US State or Territory:")
        .log()
        .add('Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida', 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Minor Outlying Islands', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Puerto Rico', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'U.S. Virgin Islands', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming')
        .print()
    ,
    newText("Type of area")
        .bold()
        .print()
    ,
    newScale("currentArea", "Rural", "Suburban", "Urban", "Unsure/Prefer not to disclose")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText("ZIP code:")
        .bold()
        .print()
    ,
    newTextInput("currentZIP")
        .settings.log()
        .settings.lines(1)
        .print()
    ,
    // Hometown
    newText("<b>Where did you grow up?<b>\n<i>If you lived in multiple places, pick one that you find most representative.</i>")
        .print()
    ,
    newDropDown("childState", "US State or Territory:")
        .log()
        .add('Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia', 'Florida', 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Minor Outlying Islands', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Puerto Rico', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'U.S. Virgin Islands', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming')
        .print()
    ,
    newText("Type of area")
        .bold()
        .print()
    ,
    newScale("childArea", "Rural", "Suburban", "Urban", "Unsure/Prefer not to disclose")
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText("ZIP code:")
        .bold()
        .print()
    ,
    newTextInput("childZIP")
        .settings.log()
        .settings.lines(1)
        .print()
    ,
    newText("Did you live in multiple regions during your childhood?")
        .bold()
        .print()
    ,
    newScale("childMultipleRegions", "Yes", "No") 
        .log()
        .settings.labelsPosition("right")
        .settings.vertical()
        .print()
    ,
    newText(" ")
        .print()
    ,
    newButton("loca-next", "Next")
        .print()
        .wait()
)

newTrial("comment_page",
    newText("Optional: please leave any final comments you have about this study.")
        .bold()
        .print()
    ,
    newTextInput("comments")
        .settings.log()
        .settings.lines(0)
        .settings.size(400, 200)
        .print()
    ,
    newButton("finish_button", "Submit Study")
        .print()
        .wait()
)

// Goodbye and response code

// let alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz"; 

// newTrial( "bye" ,
//     newText("Thank you for your participation!\nPlease enter the code below for payment in MTurk:\n\n").print(),
//     newVar("ID", "")
//         .settings.global()
//         .set(v =>[...Array(8)].reduce(a=>a+alphanum.charAt(Math.floor(Math.random()*alphanum.length)),''))
//     ,
//     newText("code", "")
//         .settings.text( getVar("ID") ) 
//         .log()
//         .print()
//     ,
//     newButton().wait()  // Wait for a click on a non-displayed button = wait here forever
// )
newTrial( "bye",
         newHtml("prolific_redirect", "prolific_redirect.html")
            .print()
        ,
      newButton().wait() // Wait for a click on a non-displayed button = wait here forever 
)
.setOption( "countsForProgressBar" , false )
// Make sure the progress bar is full upon reaching this last (non-)trial